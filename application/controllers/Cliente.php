<?php 

class Cliente extends CI_Controller{
    
    public function lista(){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        $this->load->model('ClienteModel', 'model');
        $data['tabela'] = $this->model->gera_tabela();
        $this->load->view('common/table', $data);

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');


    }

    public function detalhe($id){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        $this->load->model('ClienteModel', 'model');
        $v['cliente'] = $this->model->detalhe($id);
        $this->load->view('cliente/detalhe', $v);

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');

    }

    public function cadastro(){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        $this->load->model('ClienteModel', 'model');
        $this->model->salva_usuario();
        $this->load->view('cliente/form_cliente.php');
      

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }

    public function editar($id){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');

        //carregar 
        $this->load->model('ClienteModel', 'model');
        $data['user'] = $this->model->read($id);

        //exibir 
        $this->load->view('cliente/form_cliente', $data);

        //modificar
        $this->model->edita_usuario($id);

        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }

    public function deletar($id){
        $this->load->model('ClienteModel', 'model');
        $this->model->deletar($id);
        redirect('cliente/lista');
    }
}