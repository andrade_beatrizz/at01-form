<?php 

class Exemplo extends CI_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('exemplo/navbar');
        $this->load->view('exemplo/jumbotron');
        $this->load->view('exemplo/linha1');
        $this->load->view('exemplo/linha2');
        $this->load->view('exemplo/linha3');
        $this->load->view('exemplo/pagination');
        $this->load->view('exemplo/footer');
        $this->load->view('common/footer');
    }

    public function basico(){
        $this->load->view('common/header');
        $this->load->view('atividade/navbar');
        $this->load->view('atividade/jumbotron');
        $this->load->view('atividade/col1');
        $this->load->view('atividade/col2');
        $this->load->view('atividade/col3');
        $this->load->view('atividade/pagination');
        $this->load->view('atividade/footer');
        $this->load->view('common/footer');
    }

    public function complexo(){
        $this->load->view('common/header');
        $this->load->view('atividade/navbar');
        //conteudo da pág. inicia aqui.
        $this->load->view('exemplo/complexo/jumbo2');
        $this->load->view('exemplo/complexo/photos');
        $this->load->view('exemplo/complexo/pagination');
        $this->load->view('exemplo/complexo/footer');

        //conteudo da pág. termina aqui.
        $this->load->view('common/footer');
    }

}
?>