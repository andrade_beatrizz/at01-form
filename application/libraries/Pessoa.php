
<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Pessoa extends CI_Object {

    private $data = array(
        array('nome' =>  'José', 'sobrenome' => 'Silva', 'idade' => '27', 'sexo' => 'M' ),
        array('nome' =>  'Joana', 'sobrenome' => 'Souza', 'idade' => '29', 'sexo' => 'F' ),
        array('nome' =>  'João', 'sobrenome' => 'Santos', 'idade' => '47', 'sexo' => 'M' ),
        array('nome' =>  'Jessé', 'sobrenome' => 'Salgado', 'idade' => '21', 'sexo' => 'M' ),
        array('nome' =>  'Jordana', 'sobrenome' => 'Silveira', 'idade' => '25', 'sexo' => 'F' ),
        array('nome' =>  'Joelma', 'sobrenome' => 'Salamar', 'idade' => '12', 'sexo' => 'F' )
    );

    public function lista(){
        //selecionar os dados no bd;
        //$sql = 'SELECT * FROM pessoa';
        //$rs =  $this->db->query($sql);

        //1.retorna todos os registros da tabela pessoa
        $rs = $this->db->get('pessoa');

        //2.retorna os registro das pessoas > 80 y.
        //$condition = array('idade >' => ' 80');
        //$rs = $this->db->get_where('pessoa', $condition);
        //echo $this->db->last_query();

        //3.pessoas de 20 a 30 y.
        //$condition = array('idade >' => '19', 'idade <' => '31');
        //$rs = $this->db->get_where('pessoa', $condition);
        //echo $this->db->last_query();

        //organizar estes dados;
        $result = $rs->result_array();

        //retornar os dados
        return $result;
    }

    public function clientes(){
        $cliente[] = array(
            'foto' => 3,
            'nome' => 'José da Silva',
            'endereco' => 'R. São João, 130',
            'telefone' => '(11)24055621'
        );

        $cliente[] = array(
            'foto' => 4,
            'nome' => 'João de Souza',
            'endereco' => 'R. Bartolomeu, 130',
            'telefone' => '(11)24652445'
        );

        $cliente[] = array(
            'foto' => 5,
            'nome' => 'Maria Silva',
            'endereco' => 'R. Harris Beniccut, 230',
            'telefone' => '(11)24002010'
        );

        $cliente[] = array(
            'foto' => 12,
            'nome' => 'Joana Ribeiro',
            'endereco' => 'R. Wilsom Messias, 155',
            'telefone' => '(11)24122036'
        );
        return $cliente;
    }

    public function cria_usuario($data){
        $this->db->insert('pessoa', $data);
    }

    public function user_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('pessoa', $cond);
        return $rs->row_array();
    }
    
    public function edita_usuario($data, $id){
        $this->db->update('pessoa', $data, "id = $id");
    }

    public function delete($id){
        $cond = array('id' => $id);
        $this->db->delete('pessoa', $cond);
    }
}




