<?php 

include_once APPPATH.'libraries/Pessoa.php';
include_once APPPATH.'libraries/Costumer.php';

//classes do tipo model são geradoras dos dados que serão exibidos nas views.
class ClienteModel extends CI_Model{


    public function gera_tabela(){  
        $html = '';
        $pessoa = new Pessoa();
        $data = $pessoa->lista();

        foreach ($data as $cliente) {
            $html .= '<tr>';
            $html .= '<td>'.$cliente['id'].'</td>';
            $html .= '<td><a href="'.base_url('/cliente/detalhe/'.$cliente['id']).'">'.$cliente['nome'].'</a></td>';
            $html .= '<td>'.$cliente['sobrenome'].'</td>';
            $html .= '<td>'.$cliente['codproduto'].'</td>';
            $html .= $this->action_buttons($cliente['id']);
            $html .= '</tr>';
        }
        return $html;
    }

    private function action_buttons($id){
        $html = '<td><a href="'.base_url('cliente/editar/'.$id).'">';
        $html .= '<i class="fas fa-edit mr-2 #ff4081 pink-text accent-2" title="Editar"></i></a></td>';
        $html .= '<td><a href="'.base_url('cliente/deletar/'.$id).'">';
        $html .= '<i class="fas fa-trash mr-2 #ff4081 pink-text accent-2" title="Deletar"></i></a></td>';
        return $html;

    }

    public function detalhe($id){
        $costumer = new Costumer();
        $costumer = $costumer->lista();
        return $costumer[$id - 1];

    }

    public function salva_usuario(){
        if(sizeof($_POST) == 0) return;

        //definir regras de validação;
        $this->form_validation->set_rules('nome', 'Nome do Cliente', 'trim|required');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome do Cliente', 'trim|required');
        $this->form_validation->set_rules('codproduto', 'Código do produto', 'trim|required|is_natural|greater_than[1]|less_than[100]');
        

        //realizar a validação;
        if($this->form_validation->run()){

        //passou na validação
        //executar a ação do form.
            
        $data = $this->input->post();
        $pessoa = new Pessoa();
        $pessoa->cria_usuario($data);
        redirect('cliente/lista');
        }
    }

    public function edita_usuario($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $pessoa = new Pessoa();
        $pessoa->edita_usuario($data, $id);
        redirect('cliente/lista');
    }

    public function read($id){
        $pessoa = new Pessoa();
        return $pessoa->user_data($id);
    }

    public function deletar($id){
        $pessoa = new Pessoa();
        $pessoa->delete($id);
    }


}