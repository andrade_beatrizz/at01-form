<div class="container mt-5"></br>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/<?= $cliente['foto'] ?>.jpg" alt="Card image cap">


            <div class="card-body">
                <h4 class="card-title"><a><?= $cliente['nome'] ?>.</a></h4>
                <p class="card-text"><?= $cliente['endereco'] ?>.</p>
                <p class="card-text"><?= $cliente['telefone'] ?>.</p>
                
                

            </div>

            </div>

        </div>
    </div>
</div>