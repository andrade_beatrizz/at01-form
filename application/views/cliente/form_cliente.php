<div class="container mt-5"></br></br>

<p class="h5 mb-4 col-md-6 mx-auto #ff4081 pink-text accent-2"> Ao se cadastrar, você será redirecionado para a lista de espera.
    </p>
<?php echo validation_errors('<div class="alert alert-danger">', '</div>');?>

    <div class="row">
        <div class="col-md-6 mx-auto"></br>
            <form class="text-center border border-light p-5" method="POST">
            <p class="h4 mb-4 #ff4081 pink-text accent-2">Cadastre-se e acompanhe seu pedido.</p>
            <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" id="nome" class="form-control mb-4" placeholder="Nome do usuário" name="nome">
            <input value="<?= isset($user) ? $user['sobrenome'] : '' ?>" type="text" id="sobrenome" class="form-control mb-4" placeholder="Sobrenome do usuário" name="sobrenome">
            <input value="<?= isset($user) ? $user['codproduto'] : '' ?>" type="number" id="codproduto" class="form-control mb-4" placeholder="Código do produto" name="codproduto">
            <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Senha do usuário">
            <div class="d-flex justify-content-around">
                <div>
                   
                </div>     
            </div>
            <button class="btn btn-info btn-block my-4 #ff4081 pink accent-2" type="submit">Enviar</button>
            </form>
        </div>
    </div></br>

    <p class="h5 mb-4 col-md-6 mx-auto #ff4081 pink-text accent-2">
    Sempre que precisar, acompanhe seu pedido acessando esta lista com usuário e senha!</p>
</div>
