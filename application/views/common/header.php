<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Black Cakes - LISTA DE ESPERA</title>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css')?>" rel="stylesheet"> 
  <link href="<?= base_url('assets/mdb/css/mdb.min.css')?>" rel="stylesheet"> 
  <link href="<?= base_url('assets/css/style.css') ?> " rel="stylesheet">
  
</head>

<body>