
<footer class="page-footer font-small #3e2723 brown darken-4">


  <div class="container">


    <div class="row">


      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center">

          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg #ff4081 pink-text accent-2 mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <a class="tw-ic">
            <i class="fab fa-twitter fa-lg #ff4081 pink-text accent-2 mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <a class="ins-ic">
            <i class="fab fa-instagram fa-lg #ff4081 pink-text accent-2 mr-md-5 mr-3 fa-2x"> </i>
          </a>
        </div>
      </div>

    </div>


  </div>
  <div class="footer-copyright #ff4081 pink-text accent-2 text-center py-3">© 2019 Copyright:
    <a href="https://hospedagem.ifspguarulhos.edu.br/~gu1800337" class="#ff4081 pink-text accent-2"> Bitz.com</a>


</footer>
