<!--Grid row-->
<div class="row mt-3 wow fadeIn">

<!--Grid column-->
<div class="col-lg-5 col-xl-4 mb-4">
  <!--Featured image-->
  <div class="view overlay rounded z-depth-1">
    <img src="https://mdbootstrap.com/wp-content/uploads/2017/11/brandflow-tutorial-fb.jpg" class="img-fluid"
      alt="">
    <a href="https://mdbootstrap.com/education/tech-marketing/automated-app-introduction/" target="_blank">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>
</div>
<!--Grid column-->

<!--Grid column-->
<div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
  <h3 class="mb-3 font-weight-bold dark-grey-text">
    <strong>Bootstrap Automation</strong>
  </h3>
  <p class="grey-text">Learn how to create a smart website which learns your user and reacts properly to his
    behavior.</p>
  <a href="https://mdbootstrap.com/education/tech-marketing/automated-app-introduction/" target="_blank"
    class="btn btn-primary btn-md">Start tutorial
    <i class="fas fa-play ml-2"></i>
  </a>
</div>
<!--Grid column-->

</div>
<!--Grid row-->

<hr class="mb-5">