
<nav class="navbar fixed-top navbar-expand-lg navbar-light #3e2723 brown loghten-1 scrolling-navbar">

<a class="navbar-brand #ff4081 pink-text accent-2" href="#">Black Cakes</a>


<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
  aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="basicExampleNav">


  <ul class="navbar-nav mr-auto">
    <li class="nav-item active ">
      <a class="nav-link #ff4081 pink-text accent-2" href="cadastro">Lista de espera
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link #ff4081 pink-text accent-2" href="#">Bolos</a>
      <span class="sr-only">(current)</span>
    </li>
    <li class="nav-item">
      <a class="nav-link #ff4081 pink-text accent-2" href="#">Promocionais</a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle #ff4081 pink-text accent-2" id="navbarDropdownMenuLink" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">Outros</a>
      <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item #ff4081 pink-text accent-2" href="#">Doces</a>
        <a class="dropdown-item #ff4081 pink-text accent-2" href="#">Bolos de pote</a>
        <a class="dropdown-item #ff4081 pink-text accent-2" href="#">Cones</a>
      </div>
    </li>

  </ul>

</nav>
